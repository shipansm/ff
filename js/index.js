(function ($) {
    "use strict";
    
    /*----------------------------
        slider active
    ------------------------------ */
    var mySwiper = new Swiper('.swiper-container', {
        speed: 400,
        navigation: {
            nextEl: '.fa-angle-right',
            prevEl: '.fa-angle-left',
        },
        loop: true,
    });

    /*----------------------------
        dataTable active
    ------------------------------ */
    $('#students_table').DataTable({
        responsive: true
    });
    
})(jQuery);
